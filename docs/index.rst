.. graphconverter documentation master file, created by
   sphinx-quickstart on Mon Mar  2 18:12:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to graphconverter's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 4

   graphconverter


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

