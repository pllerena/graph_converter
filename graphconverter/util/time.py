"""
Time-related functions
"""
#from datetime import date, datetime
import bisect

from astropy.time import Time

__all__ = ['create_datetime_list', 'map_datetime', 'get_min_max_time']

def create_datetime_list(from_time, to_time, step, time_format=None):
    curr_time = from_time
    datetime_list = [curr_time]
    while curr_time <= to_time:
        curr_time = Time(curr_time.to_datetime() + step, format=time_format)
        datetime_list.append(curr_time)

    return datetime_list

def map_datetime(datetime_list, date_time):
    if date_time >= datetime_list[-1]:
        return -1
    return bisect.bisect(datetime_list, date_time)-1


def get_min_max_time(data_list, time_format, time_key):
    min_time = min(map(lambda e: Time(e.get(time_key), format=time_format), data_list))
    max_time = max(map(lambda e: Time(e.get(time_key), format=time_format), data_list))
    return min_time, max_time
