"""
Path-related functions
"""

import os

__all__ = ['next_empty_dir', 'next_empty_file']

def next_empty_dir(base_name):
    "Create a new directory and return its name, starting from a specified base dir name"
    dir_name = base_name

    num = 0

    while os.path.exists(dir_name) and len(os.listdir(dir_name)) > 0:
        # avoid rewriting previous files
        dir_name = base_name + "_" + str(num)
        num += 1

    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    return dir_name

def next_empty_file(file_name):
    "Create a new file and return its name, starting from a specified base file name"
    base_name, extension = file_name.split('.')

    num = 0

    while os.path.exists(file_name) and os.path.getsize(file_name) > 0:
        # avoid rewriting previous files
        file_name = base_name + "_" + str(num) + '.' + extension
        num += 1

    return file_name
