"""
Core functions and data structures.
"""
from .graphconverter import GraphData

__all__ = ['GraphData']
