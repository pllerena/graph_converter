"""
Defines the GraphData structure and methods.
"""
import os

from jsonschema import validate
from astropy.time import Time
import rapidjson

from graphconverter import util


class GraphData:
    """
    Data structure to build or convert dynamic graphs to multiple formats.
    """
    def __init__(self):
        self.data = None
        self.default_schema = None
        self.metadata_key = 'metadata'

    def no_data(self):
        "Obsolete method, use Boolean operator instead"
        return self.data is None

    def __bool__(self):
        "Return True if the graph is initialized or False otherwise"
        return self.data is not None

    def validate_schema(self, schema=None):
        "Validate the data with a specified json schema or the default schema"
        if schema is None and self.default_schema is None:  # use default schema
            default_schema = os.path.join(os.path.dirname(__file__),
                                          '../../graph-data-schema.json')
            with open(default_schema, 'r') as myfile:
                self.default_schema = rapidjson.loads(myfile.read())
            validate(schema=self.default_schema, instance=self.data)
        elif schema is None:
            validate(schema=self.default_schema, instance=self.data)
        elif self.default_schema is None:
            validate(schema=schema, instance=self.data)

    def init_empty(self):
        "Initialize to an empty graph"
        self.data = {self.metadata_key: {'nodes': 'nodes',
                                         'links': 'links',
                                         'time': 'time',
                                         'time_format': 'iso',
                                         'entity_type': 'entity_type'},
                     "nodes": [],
                     "links": []}

    def get_metadatum(self, key, default=None):
        "Return the metadata associated with this data"
        if self.metadata_key in self.data and key in self.data[self.metadata_key]:
            return self.data[self.metadata_key][key]
        return default

    def __getitem__(self, key):
        "Return the metadata associated with the specified key"
        return self.data[key]

    def set_metadatum(self, key, value):
        "Set a key value pair in the metadata of this data"
        if self.metadata_key not in self.data:
            self.data[self.metadata_key] = {}
        self.data[self.metadata_key][key] = value

    def get_nodes_key(self):
        "Return the nodes key for this data"
        nodes_key = self.get_metadatum('nodes', 'nodes')
        return nodes_key

    def get_links_key(self):
        "Return the links key for this data"
        links_key = self.get_metadatum('links', 'links')
        return links_key

    def get_time_key(self):
        "Return the time key for this data"
        time_key = self.get_metadatum('time')
        return time_key

    def get_nodes(self, entity_type=None):
        "Return the nodes in this data"
        nodes_key = self.get_nodes_key()
        if entity_type is None:
            return self.data[nodes_key]
        return [node for node in self.data[nodes_key] if node['entity_type'] == entity_type]

    def get_links(self):
        "Return the links in this data"
        links_key = self.get_links_key()
        return self.data[links_key]

    def get_link_keys(self):
        "Return the link keys from this data"
        link_keys = set()
        for link in self.get_links():
            link_keys = link_keys.union(set(link.keys()))

        timeslot_key = 'ts'

        keys = ['source', 'target']
        if timeslot_key in link_keys:
            keys.append(timeslot_key)

        link_keys = link_keys.difference(set(keys))
        keys.extend(link_keys)
        return keys

    def add_node(self, node):
        "Adds a node to this data"
        self.get_nodes().append(node)

    def add_link(self, link):
        self.get_links().append(link)

    def number_of_nodes(self):
        return len(self.get_nodes())

    def create_node_index(self, key):
        key = self.get_metadatum(key, key)
        keys_dict = {}
        for node in self.get_nodes():
            if key in node:
                if node[key] not in keys_dict:
                    keys_dict[node[key]] = []
                keys_dict[node[key]].append(node)
        return keys_dict

    def entity_type_index(self):
        entity_type = self.get_metadatum('entity_type')
        entity_types = self.create_node_index(entity_type)
        return entity_types

    def get_node_entity_type_set(self):
        entity_type_key = self.get_metadatum('entity_type', 'entity_type')
        return self.get_node_attributes_set(entity_type_key)

    def get_node_attributes_set(self, key):
        key = self.get_metadatum(key, key)
        attributes_set = set()
        for node in self.get_nodes():
            if key in node:
                attributes_set.add(node[key])
        return attributes_set

    def get_nodes_attributes(self, entity_type, attributes):
        entity_types = self.entity_type_index()
        node_id = "id"
        node_attribues = {node[node_id]: {attr_key: node[attr_key] for attr_key in attributes} for node in
                          entity_types[entity_type]}
        return node_attribues

    def get_nodes_time(self, entity_type):
        time_key = self.get_time_key()
        attributes = [time_key]
        return self.get_nodes_attributes(entity_type, attributes)

    def get_nodes_timeslot(self, entity_type, time_slot_key='ts'):
        attributes = [time_slot_key]
        return self.get_nodes_attributes(entity_type, attributes)

    def get_node_entity_list(self, entity_type):
        entity_type_dict = self.entity_type_index()
        nodes_list = entity_type_dict[entity_type]
        return nodes_list

    def add_ts_from_time_list_to_list(self, data_list, datetime_list, time_key, time_slot_key="ts"):
        for el in data_list:
            if time_key in el:
                el[time_slot_key] = util.time.map_datetime(datetime_list, Time(el[time_key]))

    # or should it be to entity type?
    def add_ts_to_list(self, data_list, from_time, to_time, step, time_slot_key="ts"):
        time_key = self.get_time_key()
        datetime_list = util.time.create_datetime_list(from_time, to_time, step)
        self.add_ts_from_time_list_to_list(data_list, datetime_list, time_key, time_slot_key)

    def add_from_node_dict_to_link(self, source_or_target, nodes_attr):
        for link in self.get_links():
            if link[source_or_target] in nodes_attr:
                link.update(nodes_attr[link[source_or_target]])

    def add_ts_to_links(self, from_time, to_time, step, time_slot_key="ts"):
        links_list = self.get_links()
        self.add_ts_to_list(links_list, from_time, to_time, step, time_slot_key)

    def add_ts_to_node_entity(self, entity_type, step, time_slot_key="ts"):
        self.set_metadatum("ts", time_slot_key)

        time_format = self.get_metadatum('time_format')
        time_key = self.get_time_key()

        nodes_list = self.get_node_entity_list(entity_type)
        min_time, max_time = util.time.get_min_max_time(nodes_list, time_format, time_key)

        self.add_ts_to_list(nodes_list, from_time=min_time, to_time=max_time, step=step, time_slot_key=time_slot_key)

    def to_nx_bipartite(self):
        import networkx as nx

        G = nx.Graph()
        G.add_nodes_from(map(lambda e: (e.pop("id"), e), self.get_nodes()))
        G.add_edges_from(map(lambda e: (e.pop("source"), e.pop("target"), e), self.get_links()))

        return G
