import networkx as nx
import itertools
import graphconverter
import rapidjson


def filter_dict(dictionary, attributes_to_keep):
    """Filter a dict and keep only the attributes in attributes_to_keep list."""
    new_dict = {}
    for attribute in dictionary:
        if attribute in attributes_to_keep:
            new_dict[attribute] = dictionary[attribute]
    return new_dict

def convert_json(json_data, nodes_attributes_to_keep = ["name", "id"], edges_attributes_to_keep = ["title", "ts", "id"], time_slot_key="ts", entity_type_nodes = "people", entity_type_edges = "publication", schema=None):
    """Convert one of the old json format (example : IEEE_VIS.json) into the new one"""
    G = graphconverter.GraphData()
    G.init_empty()
    G.data["metadata"]["graph_type"] = "bipartite"
        
    # loop of nodes : for each node, create one new node of set 1 (authors)
    for node in json_data["nodes"]:
        node = filter_dict(node, nodes_attributes_to_keep)
        node["entity_type"] = entity_type_nodes
        G.add_node(node)
        
    # Loop of edges : for each hyperedge, create a new node of set 2 (contracts)
    node_set2_id = "p0"
    for edge in json_data["edges"]:
        node_set2 = filter_dict(edge, edges_attributes_to_keep)
        node_set2["entity_type"] = entity_type_edges
        
        # create an id if it does not exist
        if "id" not in edge:
            node_set2["id"] = node_set2_id
            node_set2_id = node_set2_id[0] + str(int(node_set2_id[1:]) + 1)
        
        G.add_node(node_set2)
        
        # take the hyperedge and create links between the 2 sets
        for link in edge["ids"]:
            new_edge = {"source": link, "target": node_set2["id"], "ts": edge[time_slot_key]}
            G.add_link(new_edge)
    
    # Validation
#    with open('../../schema.json', 'r') as myfile:
#        G.default_schema = rapidjson.loads(myfile.read())
    G.validate_schema(schema = schema)
    #G.validate_schema(schema=schema)
    
    # return json
    return G.data


def weighted_projection(bipartite_json, projection_set="people", secondary_set="publication", is_time=True, time_key="ts", weight_key = "w"):
    """project a bipartite graph in json format in the projection set. Use networkx for the projection. time_key is the time key name used by the bipartite_json and will be used for the new unipartite graph. weight_key will be the key name used for the weight in the new graph."""
    # convert to nx graph
    G = nx.Graph(nx.json_graph.node_link_graph(bipartite_json))
    
    # unipartite graph
    G_new = nx.Graph()
    # save the metadata inside graph
    G_new.graph = bipartite_json["metadata"]
    G_new.graph["graph_type"] = "unipartite"    


    projection_set = bipartite_json["metadata"]["target_entity_type"] if bipartite_json["metadata"]["target_entity_type"] else projection_set
    secondary_set = bipartite_json["metadata"]["source_entity_type"] if bipartite_json["metadata"]["source_entity_type"] else secondary_set

    # primary node set : add nodes in the new graph with the same attributes
    nodes_set1 = [(n,d) for n, d in G.nodes(data=True) if d["entity_type"] == projection_set]
    G_new.add_nodes_from(nodes_set1)
    
    # secondary node set
    #nodes_set2 = [n for n, d in G.nodes(data=True) if d["entity_type"] == secondary_set]
    nodes_set2 = [n for n, d in G.nodes(data=True) if d["entity_type"] != projection_set]
    
    # for each contract, create a node between each possible pair of author
    for n in nodes_set2:
        neighbors_projection_set = [n2 for n2 in G[n] if G.nodes[n2]["entity_type"] == projection_set]
        node_pairs = list(itertools.combinations(neighbors_projection_set, 2))
        for pair in node_pairs:
            if is_time:
                # if we take time into account
                if pair in G_new.edges:
                    # if this time already exist for this edge, add one weight, if not initialize this time with weight one
                    if G.nodes[n][time_key] in G_new.edges[pair][weight_key]:
                        G_new.edges[pair][weight_key][G.nodes[n][time_key]] += 1
                    else:
                        G_new.edges[pair][weight_key][G.nodes[n][time_key]] = 1
                else:
                    # if edge does not exit, initialize it
                    G_new.add_edge(*pair, w={G.nodes[n][time_key]: 1})
            else:
                # Without time
                if pair in G_new.edges:
                    # if edge exist, add one weight
                    G_new.edges[pair][weight_key] += 1
                else:
                    # if edge does not exit, initialize it
                    weight = {weight_key : 1}
                    G_new.add_edge(*pair, **weight)
                
    #print(nx.json_graph.node_link_data(G_new))
    return G_new
    
    
#%% tests
if __name__ == "__main__":
    euroVisDataPath_old = "../../../Aleclust/data/paohvis/IEEE_VIS_2012_2016_InfoVis.json"
    json_data = rapidjson.loads(open(euroVisDataPath_old).read())
    new_json = convert_json(json_data)
    
    #%%
    euroVisDataPath = "../../../Aleclust/data/new_json/IEEE_VIS_2012_2016_InfoVis.json"
    json_data = rapidjson.loads(open(euroVisDataPath).read())
    G = convert_json(json_data)
    
    #Guni_ev = weighted_projection(json_data)
    
    

    #%%
    biGraphexample = "../../data/graph.json"
    json_bidata = rapidjson.loads(open(biGraphexample).read())
    Guni = weighted_projection(json_bidata, is_time=True, time_key="time")
    
    #%%
    biGraphexample2 = "../../../Aleclust/data/new_json/test.json"
    json_bidata = rapidjson.loads(open(biGraphexample2).read())
    Guni2 = weighted_projection(json_bidata, projection_set="person", is_time=True, time_key="ts")
