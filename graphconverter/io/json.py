import rapidjson

from graphconverter.core import graphconverter


def save_json(graph, file_name, indent=2):
    graph["metadata"]["format"] = "2.0"
    with open(file_name, 'wb') as myfile:
        rapidjson.dump(graph.data, myfile, indent=indent)


def load_json(file_name):
    G = graphconverter.GraphData()
    with open(file_name, 'r') as myfile:
        G.data = rapidjson.load(myfile)
    return G

#         if time_key is not None:
#         time_format = get_from_data_description('time_format')
#         data['links'] = [{time_key: Time(link.pop(time_key), format=time_format), **link} for link in data[links_key]]

#             return data
