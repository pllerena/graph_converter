"""
Manage the edge table input format.
"""
import logging
import csv

from graphconverter.core import graphconverter


COUNT = 5

def load_edge_table(edge_file_name, index_info=None, header=False, node_file_name=None):
    "Load a file in edge table format."

    G = graphconverter.GraphData()
    G.init_empty()

    def get_default_index_info():
        return {"link": {"source": 0, "target": 1}, "node": {}}

    def get_index_info(row):
        # TODO: assume source is 0, target is 1, and col name is the entity type
        index_info = {"link": dict(zip(row, range(len(row)))), "node": {}}
        if "source" not in index_info["link"]:
            logging.error("source not found in header,"
                          " please provide source and target index information")
            return None
        if "target" not in index_info["link"]:
            logging.error("target not found in header,"
                          " please provide source and target index information")
            return None
        return index_info

    def format_node(node_id, entity_type=None):
        #TODO: more complex node
        return {"id": node_id, "entity_type": entity_type}

    def format_link(row, index_info):
        return {key: row[val] for key, val in index_info.items()}


    if index_info is None and header is False:
        logging.warning("Warning... no header or column info provided,"
                        "will try default column order source: 0, target: 1")
        index_info = get_default_index_info()

    nodes_dict = {}
    with open(edge_file_name, 'r', newline='') as myfile:
        inputFile = csv.reader(myfile)
        l = 0

        if header:
            row = next(inputFile)
            logging.info(row)

            if index_info is None: # try to get source/target index_info from header
                index_info = get_index_info(row)
                if index_info is None:
                    return None

            l += 1

        for row in inputFile:
            if l<COUNT:
                logging.info(row)
                l += 1

            id_source = row[index_info["link"]["source"]]
            if id_source not in nodes_dict:
                nodes_dict[id_source] = G.number_of_nodes()
                #TODO: check/add entity type
                G.add_node(format_node(id_source, 'source'))

            id_target = row[index_info["link"]["target"]]
            if id_target not in nodes_dict:
                nodes_dict[id_target] = G.number_of_nodes()
                G.add_node(format_node(id_target, 'target'))
            G.add_link(format_link(row, index_info["link"]))

    if node_file_name:
        with open(node_file_name, 'r', newline='') as myfile:
            inputFile = csv.reader(myfile)

    return G
