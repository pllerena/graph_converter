"""
Reader for the gefx format of Gephi
"""
from xml.etree.ElementTree import ElementTree

from networkx.utils import open_file
from networkx import NetworkXError

from graphconverter.core import graphconverter




@open_file(0, mode='rb')
def read_gexf(path, node_type=int, version='1.1draft'):
    "Reads a gexf file and return a NetworkX graph"
    reader = GEXFReader(node_type=node_type, version=version)
    G = reader(path)
    return G

class GEXF:
    def __init__(self):
        self.versions = {}
        self.versions['1.1draft'] = {
            'NS_GEXF':"http://www.gexf.net/1.1draft",
            'NS_VIZ':"http://www.gexf.net/1.1draft/viz",
            'NS_XSI':"http://www.w3.org/2001/XMLSchema-instance",
            'SCHEMALOCATION':' '.join([
                'http://www.gexf.net/1.1draft',
                'http://www.gexf.net/1.1draft/gexf.xsd'
            ]),
            'VERSION':'1.1'
        }

        self.versions['1.2draft'] = {
            'NS_GEXF':"http://www.gexf.net/1.2draft",
            'NS_VIZ':"http://www.gexf.net/1.2draft/viz",
            'NS_XSI':"http://www.w3.org/2001/XMLSchema-instance",
            'SCHEMALOCATION':' '.join([
                'http://www.gexf.net/1.2draft',
                'http://www.gexf.net/1.2draft/gexf.xsd'
            ]),
            'VERSION':'1.2'
        }

        types = [
            (int, "integer"),
            (float, "float"),
            (float, "double"),
            (bool, "boolean"),
            (list, "string"),
            (dict, "string")
        ]

        try: # Python 3.x
            blurb = chr(1245) # just to trigger the exception
            types.extend([
                (int, "long"),
                (str, "liststring"),
                (str, "anyURI"),
                (str, "string")])
        except ValueError: # Python 2.6+
            types.extend([
                (long, "long"),
                (str, "liststring"),
                (str, "anyURI"),
                (str, "string"),
                (unicode, "liststring"),
                (unicode, "anyURI"),
                (unicode, "string")])

        self.xml_type = dict(types)
        self.python_type = dict(reversed(a) for a in types)
        # http://www.w3.org/TR/xmlschema-2/#boolean
        self.convert_bool = {
            'true': True, 'false': False,
            'True': True, 'False': False,
            '0': False, 0: False,
            '1': False, 1: True
        }
        self.version = None
        self.NS_GEXF = None
        self.NS_VIZ = None
        self.NS_XSI = None
        self.SCHEMALOCATION = None
        self.VERSION = None

#    try:
#        register_namespace = ET.register_namespace
#    except AttributeError:
#        def register_namespace(prefix, uri):
#            ET._namespace_map[uri] = prefix


    def set_version(self, version):
        "Set the version of this network"
        data = self.versions.get(version)
        if data is None:
            raise NetworkXError('Unknown GEXF version %s' % version)
        self.NS_GEXF = data['NS_GEXF']
        self.NS_VIZ = data['NS_VIZ']
        self.NS_XSI = data['NS_XSI']
        self.SCHEMALOCATION = data['NS_XSI']
        self.VERSION = data['VERSION']
        self.version = version
#        register_namespace('viz', d['NS_VIZ'])


class GEXFReader(GEXF):
    # Class to read GEXF format files
    # use read_gexf() function
    def __init__(self, node_type=None, version='1.1draft'):
        super(GEXFReader, self).__init__()
        self.initflags()
        self.node_type = node_type
        # assume simple graph and test for multigraph on read
        self.set_version(version)
        self.simple_graph = True
        self.xml = None
        self.timeformat = None

    def __call__(self, stream):
        self.xml = ElementTree(file=stream)
        G = self.xml.find("{%s}graph" % self.NS_GEXF)
        if G is not None:
            return self.make_graph(G)
        # try all the versions
        for version in self.versions:
            self.set_version(version)
            G = self.xml.find("{%s}graph" % self.NS_GEXF)
            if G is not None:
                return self.make_graph(G)
        raise ValueError("No <graph> element in GEXF file")

    def initflags(self):
        #node flags
        self._haspos = False
        self._hascolor = False
        self._hassize = False
        self._hasshape = False
        self._hasthickness = False
        self._edgesdynamic = False

    def make_graph(self, graph_xml):
        # start with empty DiGraph or MultiDiGraph
        edgedefault = graph_xml.get("defaultedgetype", None)
#         if edgedefault=='directed':
        G = graphconverter.GraphData()
        G.init_empty()
#         else:
#             G=nx.MultiGraph()

#         # graph attributes
        graph_start = graph_xml.get('start')
#         if graph_start is not None:
#             G.graph['start']=graph_start
        graph_end = graph_xml.get('end')
#         if graph_end is not None:
#             G.graph['end']=graph_end
        graph_mode = graph_xml.get("mode", "")
#         if graph_mode=='dynamic':
#             G.graph['mode']='dynamic'
#         else:
#             G.graph['mode']='static'

#         # timeformat
        self.timeformat = graph_xml.get('timeformat')
        if self.timeformat is None:
            self.timeformat = 'integer'

        elif self.timeformat == 'date':
            self.timeformat = 'string'

#         # node and edge attributes
        attributes_elements = graph_xml.findall("{%s}attributes"%self.NS_GEXF)
#         # dictionaries to hold attributes and attribute defaults
        node_attr = {}
        node_default = {}
        edge_attr = {}
        edge_default = {}
        for attr in attributes_elements:
            attr_class = attr.get("class")
            if attr_class == 'node':
                nattr, ndef = self.find_gexf_attributes(attr)
                node_attr.update(nattr)
                node_default.update(ndef)
#                 G.graph['node_default']=node_default
            elif attr_class == 'edge':
                eattr, edef = self.find_gexf_attributes(attr)
                edge_attr.update(eattr)
                edge_default.update(edef)
#                 G.graph['edge_default']=edge_default
#             else:
#                 raise # unknown attribute class

#         # Hack to handle Gephi0.7beta bug
#         # add weight attribute
#         ea={'weight':{'type': 'double', 'mode': 'static', 'title': 'weight'}}
#         ed={}
#         edge_attr.update(ea)
#         edge_default.update(ed)
#         G.graph['edge_default']=edge_default

#         # add nodes
        nodes_element = graph_xml.find("{%s}nodes" % self.NS_GEXF)
        if nodes_element is not None:
            for node_xml in nodes_element.findall("{%s}node" % self.NS_GEXF):
                self.add_node(G, node_xml, node_attr)

#         # add edges
        edges_element = graph_xml.find("{%s}edges" % self.NS_GEXF)
        if edges_element is not None:
            for edge_xml in edges_element.findall("{%s}edge" % self.NS_GEXF):
                self.add_link(G, edge_xml, edge_attr)

#         # switch to Graph or DiGraph if no parallel edges were found.
#         if self.simple_graph:
#             if G.is_directed():
#                 G=nx.DiGraph(G)
#             else:
#                 G=nx.Graph(G)

        if self._haspos:
            G.nodes.table.set_col_roles({'_x': '_viz_position_x',
                                         '_y': '_viz_position_y',
                                         '_z': '_viz_position_z'})
        if self._edgesdynamic:
            G.edges.table.set_col_roles({'_start_time': '_start',
                                         '_end_time': '_end'})
        #if self._hascolor: G.nodes.set_col_roles({'_x': '_viz_position_x', '_y': '_viz_position_y', '_z': '_viz_position_z'})

        return G

    def add_node(self, G, node_xml, node_attr, node_pid=None):
        # add a single node with attributes to the graph

        # get attributes and subattributues for node
        data = self.decode_attr_elements(node_attr, node_xml)
        data = self.add_parents(data, node_xml) # add any parents
        
#         if self.version=='1.1':
#             data = self.add_slices(data, node_xml)  # add slices
#         else:
#             data = self.add_spells(data, node_xml)  # add spells
        data = self.add_viz(data, node_xml) # add viz
#         data = self.add_start_end(data, node_xml) # add start/end


        # find the node id and cast it to the appropriate type
        node_id = node_xml.get("id")
        if self.node_type is not None:
            node_id=self.node_type(node_id)
            
        # every node should have a label
        node_label = node_xml.get("label")
        data['label']=node_label

        # parent node id
        node_pid = node_xml.get("pid", node_pid)
        if node_pid is not None:
            data['pid']=node_pid
            
        # check for subnodes, recursive
#         subnodes=node_xml.find("{%s}nodes" % self.NS_GEXF)
#         if subnodes is not None:
#             for node_xml in subnodes.findall("{%s}node" % self.NS_GEXF):
#                 self.add_node(G, node_xml, node_attr, node_pid=node_id)


        G.add_node({"id": node_id})#, data)


    def add_parents(self, data, node_xml):
        parents_element = node_xml.find('{%s}parents' % self.NS_GEXF)
        if parents_element is not None:
            data['parents'] = []
            for p in parents_element.findall('{%s}parent' % self.NS_GEXF):
                parent = p.get('for')
                data['parents'].append(parent)
        return data


    def add_start_end(self, data, xml):
        # start and end times
        ttype = self.timeformat

        node_start = xml.get('start')
        if node_start is not None:
            data['start'] = self.python_type[ttype](node_start)
        node_end = xml.get('end')
        if node_end is not None:
            data['end'] = self.python_type[ttype](node_end)
        return data

    def add_viz(self, data, node_xml):
        # add viz element for node
        viz = {}        
        color = node_xml.find('{%s}color' % self.NS_VIZ)
        if color is not None:
            if self.VERSION == '1.1':
                viz['color'] = {'r': int(color.get('r')),
                                'g': int(color.get('g')),
                                'b': int(color.get('b'))}
            else:
                viz['color'] = {'r': int(color.get('r')),
                                'g': int(color.get('g')),
                                'b': int(color.get('b')),
                                'a': float(color.get('a', 1))}
            self._hascolor = True

        size = node_xml.find('{%s}size' % self.NS_VIZ)
        if size is not None:
            viz['size'] = float(size.get('value'))
            self._hassize = True

        thickness = node_xml.find('{%s}thickness' % self.NS_VIZ)
        if thickness is not None:
            viz['thickness'] = float(thickness.get('value'))
            self._hasthickness = True

        shape = node_xml.find('{%s}shape' % self.NS_VIZ)
        if shape is not None:
            viz['shape'] = shape.get('shape')
            if viz['shape'] == 'image':
                viz['shape'] = shape.get('uri')
                self._hasshape = True

        position = node_xml.find('{%s}position' % self.NS_VIZ)
        if position is not None:
            viz['position'] = {'x': float(position.get('x', 0)),
                               'y': float(position.get('y', 0)),
                               'z': float(position.get('z', 0))}
            self._haspos = True

        if len(viz) > 0:
            data['viz'] = viz
        return data

    def add_link(self, G, edge_element, edge_attr):
        # add an edge to the graph

        # raise error if we find mixed directed and undirected edges
        edge_direction = edge_element.get('type')
        # if G.is_directed() and edge_direction == 'undirected':
        #     raise nx.NetworkXError(
        #         'Undirected edge found in directed graph.')
        # if (not G.is_directed()) and edge_direction == 'directed':
        #     raise nx.NetworkXError(
        #         'Directed edge found in undirected graph.')

        # Get source and target and recast type if required
        source = edge_element.get('source')
        target = edge_element.get('target')
        if self.node_type is not None:
            source = self.node_type(source)
            target = self.node_type(target)

        data = self.decode_attr_elements(edge_attr, edge_element)
        data = self.add_start_end(data, edge_element)

        # if self.version == '1.1':
        #     data = self.add_slices(data, edge_element)  # add slices
        # else:
        #     data = self.add_spells(data, edge_element)  # add spells

        # GEXF stores edge ids as an attribute
        # NetworkX uses them as keys in multigraphs
        # if networkx_key is not specified as an attribute
        edge_id = edge_element.get('id')
        if edge_id is not None:
            data['id'] = edge_id

        # check if there is a 'multigraph_key' and use that as edge_id
        multigraph_key = data.pop('networkx_key', None)
        if multigraph_key is not None:
            edge_id = multigraph_key

        weight = edge_element.get('weight')
        if weight is not None:
            data['weight'] = float(weight)

        edge_label = edge_element.get('label')
        if edge_label is not None:
            data['label'] = edge_label

        # start and end times
        ttype = self.timeformat

        if 'start' in data and 'end' in data:
            data['start'] = self.python_type[ttype](data['start'])
            data['end'] = self.python_type[ttype](data['end'])
            self._edgesdynamic = True

        # if G.has_edge(source, target):
            # seen this edge before - this is a multigraph
            # self.simple_graph = False

        G.add_link({"source": source, "target": target})#, data)
        # if edge_direction == 'mutual':
        # G.add_edge(target, source, key=edge_id, **data)
    
    def decode_attr_elements(self, gexf_keys, obj_xml):
        # Use the key information to decode the attr XML
        attr = {}
        # look for outer '<attvalues>' element
        attr_element = obj_xml.find('{%s}attvalues' % self.NS_GEXF)
        if attr_element is not None:
            # loop over <attvalue> elements
            for a in attr_element.findall('{%s}attvalue' % self.NS_GEXF):
                key = a.get('for')  # for is required
                try:  # should be in our gexf_keys dictionary
                    title = gexf_keys[key]['title']
                except KeyError:
                    raise nx.NetworkXError('No attribute defined for=%s.' % key)
                atype = gexf_keys[key]['type']
                value = a.get('value')
                if atype == 'boolean':
                    value = self.convert_bool[value]
                else:
                    value = self.python_type[atype](value)
                if gexf_keys[key]['mode'] == 'dynamic':
                    # for dynamic graphs use list of three-tuples
                    # [(value1,start1,end1), (value2,start2,end2), etc]
                    ttype = self.timeformat
                    start = self.python_type[ttype](a.get('start'))
                    end = self.python_type[ttype](a.get('end'))
                    if title in attr:
                        attr[title].append((value, start, end))
                    else:
                        attr[title] = [(value, start, end)]
                else:
                    # for static graphs just assign the value
                    attr[title] = value
        return attr
    
    def find_gexf_attributes(self, attributes_element):
        # Extract all the attributes and defaults
        attrs = {}
        defaults = {}
        mode=attributes_element.get('mode')
        for k in attributes_element.findall("{%s}attribute" % self.NS_GEXF):
            attr_id = k.get("id")
            title=k.get('title')
            atype=k.get('type')
            attrs[attr_id]={'title':title,'type':atype,'mode':mode}
            # check for the "default" subelement of key element and add
            default=k.find("{%s}default" % self.NS_GEXF)
            if default is not None:
                if atype=='boolean':
                    value=self.convert_bool[default.text]
                else:
                    value=self.python_type[atype](default.text)
                defaults[title]=value
        return attrs,defaults
