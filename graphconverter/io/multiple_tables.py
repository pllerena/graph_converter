import os
import csv

from graphconverter.core import graphconverter
from graphconverter import util


def get_link_node(link, node_type, nodes_dict=None, node_attribute_key=None):
    node_id = link[node_type]    
    if nodes_dict is not None and node_attribute_key is not None:
        if node_id in nodes_dict.keys(): # this is useful for replacing a node id with a node attribute
            node_attribute = nodes_dict[node_id]#[node_attribute_key]  
            return node_attribute
        else:
            warnings.warn("Warning... {} \"{}\" in link list not in nodes object".format(node_type, link[node_type])) 
    return node_id

# save edge table
def get_row(entity, row_fields, nodes_dict_source=None, nodes_dict_target=None): # TODO: get_link_node

    row = []

    for field in row_fields:
        info = ''
        if field in entity:
            if field == 'source' and nodes_dict_source is not None:
                info = get_link_node(entity, field, nodes_dict_source, "id")
            elif field == 'target' and nodes_dict_target is not None:
                info = get_link_node(entity, field, nodes_dict_target, "id")
            else:
                info = entity[field]
        row.append(info)

    return row

def get_link_row(link, row_fields=["source", "target", "time"], nodes_dict_source=None, nodes_dict_target=None):
    return get_row(link, row_fields, nodes_dict_source, nodes_dict_target)

def get_node_row(link, row_fields=["id"]):
    return get_row(link, row_fields)

def save_edge_table(graph, file_name, row_fields=["source", "target"], header=False, 
                    replace_source_id_with=None, replace_target_id_with=None):
    nodes_dict_source = None 
    nodes_dict_target = None 
    
    link_keys = graph.get_link_keys()

    if replace_source_id_with is not None: # TODO: check that all have this !!
        nodes_dict_source = {d["id"]: d[replace_source_id_with] for d in graph.get_nodes()}

    if replace_target_id_with is not None: # TODO: check that all have this !!
        nodes_dict_target = {d["id"]: d[replace_target_id_with] for d in graph.get_nodes()}

    with open(file_name, 'w') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        if header:
            wr.writerow(row_fields)
        wr.writerows([get_link_row(link, row_fields, nodes_dict_source, nodes_dict_target) 
            for link in graph.get_links()])


def save_nodes_table(graph, node_list, file_name, row_fields, header=False):
    
    with open(file_name, 'w') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        if header:
            wr.writerow(row_fields)
        wr.writerows([get_node_row(node, row_fields) for node in node_list])

def save_all_tables(graph, dir_name, edges_file_name='edge_table.csv', header=False, row_link_fields=None,
                    replace_source_id_with=None, replace_target_id_with=None):

    dir_name = util.path.next_empty_dir(dir_name)

    entity_type_key = graph.get_metadatum("entity_type")

    entity_type_dict = graph.entity_type_index()


    for entity_type in entity_type_dict:

        file_name = os.path.join(dir_name, entity_type + '.csv')

        node_list = entity_type_dict[entity_type]
        if len(node_list) > 0:
            row_all = set(node_list[0].keys())
            if entity_type_key in row_all:
                row_all.remove(entity_type_key)
            row_all = ["id"] + list(row_all.difference(set(["id"])))
    #         print(row_all)
            save_nodes_table(graph, node_list, file_name, row_all, header)

    if row_link_fields is None: row_link_fields = graph.get_link_keys() # save them all

    save_edge_table(graph, os.path.join(dir_name, edges_file_name), row_link_fields, header,
        replace_source_id_with, replace_target_id_with)

    
def load_nodes_csv(graph, file_name, entity_type=None):
    if not graph:
        graph.init_empty()

    if entity_type is None: # entity_type will be taken from the file name
        entity_type = os.path.basename(file_name).split('.')[0]
        
    entity_type_key = graph.get_metadatum("entity_type")

    nodes_key = graph.get_nodes_key()
    with open(file_name) as fh:
            for row in csv.DictReader(fh, delimiter=','):
                row[entity_type_key] = entity_type
                graph.data[nodes_key].append(dict(row))

def load_node_tables_csv(graph, nodes_files):
    if not graph:
        graph.init_empty()
        
    for file_name in nodes_files:
        load_nodes_csv(graph, file_name)

def load_edge_table_csv(graph, edges_file_name):
    if not graph:
        graph.init_empty()

    links_key = graph.get_links_key()
    
    with open(edges_file_name) as fh:
        for row in csv.DictReader(fh, delimiter=','):
            graph.data[links_key].append(dict(row))
            
def load_tables_csv(nodes_files, edges_file_name):
    graph = graphconverter.GraphData()
    graph.init_empty()
    load_node_tables_csv(graph, nodes_files)
    load_edge_table_csv(graph, edges_file_name)
    return graph
