# graph_converter

`graph_converter` is a library to convert dynamic graphs between multiple file formats.
It is a deliverable from the European project [IVAN](https://project.inria.fr/ivan/).

## Installation

`graph_converter` can be installed with the following command:

``` sh
python setup.py install
```

## License

The project is licensed under the BSD license.

