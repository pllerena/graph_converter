"""
Installation script for the graphconverter package.
"""
import setuptools

setuptools.setup(
    name="graphconverter", # Replace with your own username
    version="0.0.1",
    author="Paola Valdivia",
    author_email="paoyo1@gmail.com",
    url="https://gitlab.inria.fr/pllerena/graph_converter",
    description="graph converter package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        "networkx",
        "astropy",
        "python-rapidjson",
        "jsonschema"
    ],
)
